# Test Launch Guide

This document explains how to execute various tests from different Git repositories or CINES agent projects.

## adastra-reg-tests

1. Log in to a node with internet access (login0 / login9).
2. Navigate to the `adastra-reg-tests` directory.
3. Run the `launch_test.sh` script to execute the current functional tests:

```
cd adastra-reg-tests
./launch_test.sh
```

## cines-reframe-tests

### Running the Tests

1. Log in to a node with internet access (login0 / login9). (The following script clones the Gaia repository.)
2. Navigate to the `cines-reframe-tests` directory.
3. Run the `launch_tests.sh` script to execute the currently functional tests via Reframe:

```
cd cines-reframe-tests
./launch_tests.sh
```

### Checking the Results

The `launch_tests.sh` script executes Reframe tests for programming environments. It generates one output files: `prg-env-test.log`,  prefixed with the date and time of test execution in the "YEAR-MONTH-DAY-HOUR" format.

To check the results, open the output files, we should have the following output
```
[==========] Running 1 check(s)
[==========] Started on Thu Apr 11 15:10:06 2024+0200
[----------] start processing checks
[ RUN      ] CrayVariablesCheckAdastra %cray_module=cray-hdf5-parallel /9c3f3e05 @adastra:login+builtin
[       OK ] (1/1) CrayVariablesCheckAdastra %cray_module=cray-hdf5-parallel /9c3f3e05 @adastra:login+builtin
[----------] all spawned checks have finished
[  PASSED  ] Ran 1/1 test case(s) from 1 check(s) (0 failure(s), 0 skipped, 0 aborted)
[==========] Finished on Thu Apr 11 15:10:06 2024+0200
```

except for the following tests which are either incorrectly configured or the references have not been updated:
 - OneThreadPer*
 - OneTaskPer* 
 - AlternateSocketFilling
 - LoginEnvCheck
 - MpiInitTest
 - SSHLoginEnvCheck
 - UlimitCheck

# bench-ai-tests

## Llama-2-tests

### Running the Tests

1. Log in to a node with internet access (login0 / login9).
2. a) Llama-2 tests are include in the `launch_all.sh` script. 

```
./launch_all.sh
```
2. b) If you want to launch only Llama-2 use the script below :
```
cd bench-ai/Llama-2
sh create_venv.sh
cd Llama-2-7b
sh prepare.sh
sbatch run.slurm
cd ../../..
```

### Checking the Results

You will find below reference results for the best configuration :
*Llama-2-7b* :
n*GPU|Optimized DDP|ZeRO stage|Batch size/gpu|Epoch duration (s)|Training tokens/s|Inference tokens/s|GPU memory allocated(GB)|Avg loss|perplexity|
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|8|deepspeed|2|3|444.36|10826|26124|49.14|0.57|1.64|

You should have similar results with maximum about 10% of difference.


