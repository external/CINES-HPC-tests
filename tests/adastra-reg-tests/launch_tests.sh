git clone https://depot.cines.fr/dci/exploitation/adastra-reg-tests.git
cd adastra-reg-tests/
git checkout 16-reg_test_synth_compute_hpcg_gpu
cd SYNTH/COMPUTE/HPCG_GPU/
./prepare.sh
./run.sh
./validate.sh
cd -
cd SYNTH/COMPUTE/HPL_GPU/
./prepare.sh
./run.sh
./validate.sh
cd -
cd SYNTH/INTERCONNECT/SMB/
./prepare.sh
./run.sh
./validate.sh
cd -
cd SYNTH/INTERCONNECT/TT/
./prepare.sh
./run.sh
./validate.sh
cd -
cd SYNTH/IO/IOR_MDtest/
./prepare.sh
./run.sh
./validate.sh
cd -
cd SYNTH/JOB/TIMETOSTART/
./prepare.sh
./run.sh
./validate.sh
cd -

