#!/bin/bash

if [ -d "gaia" ]; then
    cd gaia
    git pull
else
    git clone --recursive https://depot.cines.fr/dci/rnd/gaia.git
    cd gaia
fi
source setup_env
DATE=`date +"%Y-%m-%d-%H"`

# Testing the Programming Environment  :
if [ -n "$RESERVATION_NAME" ]; then
    reframe -R -c reframe/cines-checks/prgenv/ --keep-stage-files -r -J reservation=$RESERVATION_NAME &> ${DATE}-prg-env-test.log
    mv ${DATE}-prg-env-test.log ../.
else
    echo "La variable RESERVATION_NAME doit être définie."
fi
# Application testing:
# We test all the CINES environments, but this does not fall within the scope of checking that the machine is working properly.
# reframe -R -c reframe/cines-checks/apps/ --keep-stage-files -r &> ${DATE}-apps-test.log

# Libraries testing:
# Out of scope
# reframe -R -c reframe/cines-checks/libraries --keep-stage-files -r &> ${DATE}-libraries-test.log

cd -

