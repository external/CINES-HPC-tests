#!/bin/bash

# tests SLURM
cd slurm/regtests
./launchCINES.tests.sh
cd ../..

# tests bench-ai
## Llama-2
cd bench-ai/Llama-2
sh create_venv.sh
cd Llama-2-7b
sh prepare.sh
sbatch run.slurm
cd ../../..

# cines-reframe-tests
cd cines-reframe-tests
./launch_tests.sh
cd ..

# adastra-reg-tests
cd adastra-reg-tests
./launch_tests.sh
cd ..