# CINES HPC Tests

Welcome to the repository of HPC tests from the French National Computing Center for Higher Education (CINES).

This repository serves as a central collection point for various HPC tests from different projects and repositories (considered public) managed by the French National Computer Centre for Higher Education (CINES).

These tests are intended to cover various aspects of HPC infrastructures, including:

- Performance
- Memory usage
- I/O (input/output)
- Network communications
- System reliability and stability

## Repository Structure

The repository is organized as follows:

- `tests/`: Contains test scripts and configuration files.
- `tests/adastra-reg-tests`: Contains test scripts (only synthetic script for now) 
- `tests/bench-AI`: Contains bench-AI test scripts.
- `tests/cines-reframe-tests`: Contains test scripts (only for programmming environnement for now).
- `tests/slurm`: Contains test scripts for SLURM job manage.
<!-- - `docs/`: Additional documentation on tests and their usage.
- `data/`: Supplementary test data required for test execution.
- `contrib/`: External contributions and assistance scripts.

## Contribution

We encourage contributions from the HPC community to the development and improvement of these tests. If you would like to contribute, please refer to the contribution guide in the `contrib/` directory.

## Usage

To use these tests, follow the detailed instructions in the `docs/` directory.-->

## Getting started

### Clone the repository
```
git clone --recursive https://dci-gitlab.cines.fr/external/CINES-HPC-tests.git
```

### Run the tests
Either run all the tests with the `launch_all.sh` script (which has never been tested!)
```
cd tests
./launch_all.test.sh
```
Or we run them one by one, reexecuting the script line by line `launch_all.sh`

For more information on the launch of tests and the validation phase, please refer to the [README in the tests folder}(https://git.cines.fr/external/CINES-HPC-tests/-/blob/develop/tests/README.md?ref_type=heads).
## Contact

For any questions or comments, feel free to contact the CINES team at the following address: [svp@cines.fr](mailto:svp@cines.fr).
